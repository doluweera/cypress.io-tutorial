class SignInPage {
    visit() {
      cy.visit('https://app.asana.com/-/login');
    }

    fillEmail(value) {
      const field = cy.get(`input#email_input`);
      field.type(value);
      
      return this;
    }
  
    fillPassword(value) {
      const field = cy.get(`input#password_input`);
      field.type(value);
      
      return this;
    }
    
    submit() {
      const button = cy.get(`#submit_button.buttonView.buttonView--default.buttonView--primary.buttonView--large.button-submit`);
      button.click();
    }
   
    clickProfileSettings() {
      const link = cy.get('a.TopbarPageHeaderGlobalActions-settingsMenuButton');
      link.click();
    }

    myProfileSettings() {
      const link1 = cy.get('a.MenuItemBase-button.MenuItemBase--medium.Menu-menuItem').eq(2);
      link1.click
    }
    
  }
  
  export default SignInPage;