import SignInPage from '../pages/SignInPage';

describe('ASANA', () => {

it('should sign in with correct credentials', () => {
    const signIn = new SignInPage();
    signIn.visit();

    signIn
      .fillEmail('testers123mail@gmail.com')
      .fillPassword('tester@asana123')
      .submit()
  });

  // it('should visit profile settings', () => {
  //   const signIn = new SignInPage();
  //   signIn.visit();

  //   signIn
  //     .fillEmail('testers123mail@gmail.com')
  //     .fillPassword('tester@asana123')
  //     .submit()
  //     .clickProfileSettings()
  //     // .myProfileSettings()
  // });
});