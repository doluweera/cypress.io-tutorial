describe('Asana Page', function() {

    it('file upload', function() {

      cy.visit('https://app.asana.com/-/login')  //goto site
   
      cy.get('input#email_input')
        .type('testers123mail@gmail.com')
      cy.get('input#password_input')
        .type('tester@asana123')
      cy.wait(2000)  
      cy.get('#submit_button.buttonView.buttonView--default.buttonView--primary.buttonView--large.button-submit')
        .click()
      cy.waitUntil(() => cy.get("a.TopbarPageHeaderGlobalActions-settingsMenuButton"))
    //   cy.get('a.TopbarPageHeaderGlobalActions-settingsMenuButton')
        .click()
      cy.get('a.MenuItemBase-button.MenuItemBase--medium.Menu-menuItem').eq(2) 
        .click()
      cy.get('span.primary-title').should('contain','My Profile Settings') 

      const fileName = 'funny_bunny.png';
 
      cy.fixture(fileName).then(fileContent => {
      cy.get('input.PhotoUpload-hiddenFileInput').upload({ fileContent, fileName, mimeType: 'image/png' });

    //   cy.waitUntil(() => cy.get("button.AvatarInput-removeButton.LinkButton"))
      cy.wait(15000)
      cy.get('button.PhotoUpload-removeButton.LinkButton')
        .click();
      cy.get('label.Switch-label')
        .click()
         
      cy.get('input.textInput.textInput--large.DateInput-textInput').eq(0)
        .click()
      cy.wait(2000)
      cy.get('a.tabView-tabName').eq(2)
        .click()
      cy.get('select#task-creation-domain-1133495906124287.task-creation-domain-selector')
        .select('salpo.com')
      cy.get('.dialogView2-closeX.borderless-button')
        .click() 
      cy.get('a.TopbarPageHeaderGlobalActions-settingsMenuButton')
        .click()
      cy.get('a.MenuItemBase-button.MenuItemBase--medium.Menu-menuItem').eq(3) 
        .click() 
      cy.get('h1.title').should('contain','Log In')   

      }); 
    })  
})    