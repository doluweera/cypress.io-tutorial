describe('The Home Page', function() {

    it('successfully login', function() {

      cy.visit('https://reactjs.org/')  //goto site
      cy.get('a.css-1053yfl').eq(0)
        .click()
      cy.get('div.css-1n7kyh0').eq(0)  //click links
        .click()
      cy.get('a.css-19pur11').eq(3)
        .click()
      cy.get('h1').should('contain','Hello World') 
      cy.url().should('eq','https://reactjs.org/docs/hello-world.html')  //assertions
      cy.get('a').should('contain','practical tutorial')
      cy.wait(3000)
      cy.get('input#algolia-doc-search.css-7dpbpx.ds-input')  //search
        .type('Using Web Components in React{enter}')
      
      cy.title().should('eq','Hello World – React')  //get title
      cy.get('a.css-4ivotw')
        .click()
      
      cy.get('label.css-7qz0p2 input').eq(0)
        .click()
      cy.get('label.css-7qz0p2 input').eq(1).should('have.attr', 'checked')
    })
})